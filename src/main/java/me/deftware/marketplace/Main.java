package me.deftware.marketplace;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import me.deftware.client.framework.main.preprocessor.ModPreProcessor;
import me.deftware.client.framework.utils.WebUtils;
import me.deftware.marketplace.specialized.NullByteSpecializer;
import me.deftware.marketplace.specialized.OptiFineSpecializer;
import me.deftware.marketplace.specialized.Specializer;
import me.deftware.marketplace.validate.EMCModValidator;
import me.deftware.marketplace.validate.FabricModValidator;
import me.deftware.marketplace.validate.Validator;

import java.util.Arrays;
import java.util.List;

/**
 * @author Deftware
 */
public class Main extends ModPreProcessor {

	/**
	 * An index of all public marketplace mods
	 */
	public static final String endpoint = "https://gitlab.com/EMC-Framework/maven/raw/master/marketplace/index.json";

	private final List<Validator> validatorList = Arrays.asList(new EMCModValidator(), new FabricModValidator());
	private final List<Specializer> specializerList = Arrays.asList(new OptiFineSpecializer(), new NullByteSpecializer());
	public static JsonArray modsArray;
	public static Main INSTANCE;

	@Override
	public String getName() {
		return "Aristois PreProcessor";
	}

	@Override
	public void run() {
		if (preProcessor.getEmcJar() == null) return;
		INSTANCE = this;
		try {
			JsonObject marketplaceJson = new Gson().fromJson(WebUtils.get(endpoint), JsonObject.class);
			modsArray = marketplaceJson.get("mods").getAsJsonArray();
		} catch (Exception ex) {
			return;
		}
		for (Validator validator : validatorList) {
			for (Validator.IValidatedFile file : validator.compute()) {
				try {
					if (!file.getComputedHash().equalsIgnoreCase(file.getExpectedHash())) {
						file.updateFile(file.getFile());
					}
				} catch (Exception ignored) {
				}
			}
		}
		for (Specializer specializer : specializerList) {
			try {
				if (!specializer.execute()) {
					log(specializer.getClass().getName() + " failed to execute");
				}
			} catch (Exception ignored) { }
		}
	}

}
