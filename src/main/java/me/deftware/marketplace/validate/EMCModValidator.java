package me.deftware.marketplace.validate;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import me.deftware.client.framework.utils.HashUtils;
import me.deftware.marketplace.Main;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Stream;

/**
 * Validates the sha1 of installed marketplace mods
 *
 * @author Deftware
 */
public class EMCModValidator extends Validator {

	@Override
	public Stream<File> getFiles() {
		return Arrays.stream(Objects.requireNonNull(Main.INSTANCE.preProcessor.getEMCModsDir().listFiles()))
				.filter(file -> file.isFile() && file.getName().endsWith(".jar"));
	}

	@Override
	public List<IValidatedFile> compute() {
		List<IValidatedFile> validatedFiles = new ArrayList<>();
		getFiles().forEach(file -> {
			AtomicReference<JsonObject> fileJson = new AtomicReference<>(null);
			for (JsonElement element : Main.modsArray) {
				JsonObject object = element.getAsJsonObject();
				if ((object.get("id").getAsString() + ".jar").equalsIgnoreCase(file.getName())) {
					fileJson.set(object);
					break;
				}
			}
			if (fileJson.get() != null) {
				validatedFiles.add(new IValidatedFile() {

					@Override
					public File getFile() {
						return file;
					}

					@Override
					public String getComputedHash() throws Exception {
						return HashUtils.getSha1(file);
					}

					@Override
					public String getExpectedHash() {
						return fileJson.get().get("sha1").getAsString();
					}

					@Override
					public void updateFile(File file) {
						Main.INSTANCE.log("Marking " + file.getName() + " for updating");
						if (!file.renameTo(new File(file.getAbsolutePath() + ".reinstall"))) {
							Main.INSTANCE.log("Failed to rename file " + file.getName());
						}
					}

				});
			}
		});
		return validatedFiles;
	}

}
