package me.deftware.marketplace.validate;

import java.io.File;
import java.util.List;
import java.util.stream.Stream;

/**
 * @author Deftware
 */
public abstract class Validator {

	public abstract Stream<File> getFiles();

	public abstract List<IValidatedFile> compute();

	public interface IValidatedFile {

		File getFile();

		String getComputedHash() throws Exception;

		String getExpectedHash();

		void updateFile(File file);

	}

}
