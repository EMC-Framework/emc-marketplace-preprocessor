package me.deftware.marketplace.validate;

import me.deftware.marketplace.Main;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

/**
 * Validates the sha1 of Fabric mods installed from the marketplace
 *
 * @author Deftware
 */
public class FabricModValidator extends Validator {

	@Override
	public Stream<File> getFiles() {
		return Arrays.stream(Objects.requireNonNull(Main.INSTANCE.preProcessor.getEmcJar().getParentFile().listFiles()))
				.filter(file -> file.isFile() && file.getName().endsWith(".jar"));
	}

	/**
	 * TODO
	 */
	@Override
	public List<Validator.IValidatedFile> compute() {
		List<Validator.IValidatedFile> validatedFiles = new ArrayList<>();
		/*for (File file : (File[]) getFiles().toArray()) {
			validatedFiles.add(new Validator.IValidatedFile() {

				@Override
				public File getFile() {
					return null;
				}

				@Override
				public String getComputedHash() {
					return null;
				}

				@Override
				public String getExpectedHash() {
					return null;
				}

				@Override
				public void updateFile(File file) {

				}

			});
		}*/
		return validatedFiles;
	}

}
