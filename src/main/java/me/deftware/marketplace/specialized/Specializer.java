package me.deftware.marketplace.specialized;

/**
 * @author Deftware
 */
public interface Specializer {

	boolean execute();

}
