package me.deftware.marketplace.specialized;

import me.deftware.marketplace.Main;

import java.io.File;

/**
 * Validates the file size of Fabric mods
 *
 * @author Deftware
 */
public class NullByteSpecializer implements Specializer {

	@Override
	public boolean execute() {
		File[] files = Main.INSTANCE.preProcessor.getEmcJar().getParentFile().listFiles();
		if (files == null) return false;
		for (File file : files) {
			try {
				if (file.exists()) {
					if (file.length() < 3) {
						Main.INSTANCE.log("Removed invalid fabric mod " + file.getName());
						if (!file.delete()) {
							Main.INSTANCE.log("Failed to delete " + file.getName());
						}
					}
				}
			} catch (Throwable ignored) {}
		}
		return true;
	}

}
