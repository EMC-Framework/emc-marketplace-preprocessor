package me.deftware.marketplace.specialized;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import me.deftware.client.framework.path.OSUtils;
import me.deftware.client.framework.utils.HashUtils;
import me.deftware.client.framework.utils.WebUtils;
import me.deftware.marketplace.Main;

import java.io.File;

/**
 * Validates the checksum of OptiFine and OptiFabric
 *
 * @author Deftware
 */
public class OptiFineSpecializer implements Specializer {

	@Override
	public boolean execute() {
		try {
			String dataUrl = "https://gitlab.com/EMC-Framework/maven/raw/master/marketplace/plugins/optifine/versions.json";
			File optifabricJar = new File(Main.INSTANCE.preProcessor.getEmcJar().getParentFile().getAbsolutePath() + File.separator + "optifabric.jar");
			File optifineModJar = new File(OSUtils.getMCDir() + File.separator + "mods" + File.separator + "optifine.jar");
			JsonObject json = new Gson().fromJson(WebUtils.get(dataUrl), JsonObject.class);
			if (json.has(Main.INSTANCE.preProcessor.getMinecraftVersion())) {
				JsonObject data = json.get(Main.INSTANCE.preProcessor.getMinecraftVersion()).getAsJsonObject();
				if (data.get("available").getAsBoolean()) {
					if (optifineModJar.exists()) {
						if (!data.get("sha1").getAsString().equalsIgnoreCase(HashUtils.getSha1(optifineModJar))) {
							Main.INSTANCE.log("OptiFine out of date, removing");
							if (!optifineModJar.delete()) {
								Main.INSTANCE.log("Failed to delete optifine jar");
							}
							if (optifabricJar.exists()) {
								if (!optifabricJar.delete()) {
									Main.INSTANCE.log("Failed to delete optifabric jar");
								}
							}
						}
					} else if (optifabricJar.exists()) {
						if (!optifabricJar.delete()) {
							Main.INSTANCE.log("Failed to delete optifabric jar");
						}
					}
				}
			}
		} catch (Exception ignored) { }
		return true;
	}

}
